'''class Add:
    c=10
    def display(self):
        print('class add')
obj=Add()
print(obj.c)
obj.display()'''


'''class Add:
    c=10
    def display(self):# self repreasenting current obj
        print('class add')
obj=Add()
print(obj.c)
obj.display()'''

class Add:
    c=10
    def display(self):# self repreasenting current obj
        print(self.c)
   
obj=Add()
print(obj.c)
obj.display()


'''class Add:
    c=10
    def display(self):# self repreasenting current obj
        print(self.c)
    def check(self):
        self.display()
obj=Add()
#print(obj.c)
obj.check()'''

'''class Add:
    c=10
    def display(self):# self repreasenting current obj
       
        #a=20
        self.a=20
        print(self.c)
    def check(self):
        print(self.a)
obj=Add()
obj.display()
print(obj.a)'''


'''class Add:
    c=10
    def display(self,x,y):# self repreasenting current obj
        print(x+y)
        #a=20
        self.a=20
        print(self.c)
    def check(self):
        print(self.a)
obj=Add()
obj.display(6,9)
print(obj.a)'''

'''class Add:
    #c=10
    def display(self,x,y):# self repreasenting current obj
        self.x=x
        self.y=y
        print(x+y)
       
    def check(self):
       
        print(self.x*self.y)
obj=Add()
obj.display(6,9)
obj.check()'''




'''class Add:
    c=10
    def display(self):# self repreasenting current obj
        #a=20
        self.a=10
        print(self.x)
    def check(self):
        print(self.a)
obj=Add()
#print(obj.c)
obj.x=12
obj.display()'''


'''class Add:
    c=10
    def __init__(self):
        print('ok')
    def display(self,x,y):# self repreasenting current obj
        self.x=x
        self.y=y
        print(x+y)
       
    def check(self):
       
        print(self.x*self.y)
obj=Add()
obj.display(6,9)
obj.check()'''

'''class Add:
    c=10
    def __init__(self,name,marks):#default constructor
        
        print(name,marks)
        
        print('ok')
    def display(self,x,y):# self repreasenting current obj
        self.x=x
        self.y=y
        print(x+y)
       
    def check(self):
       
        print(self.x*self.y)
obj=Add('ss',55)
obj.display(6,9)
obj.check()'''


'''class Add:
    c=10
    def __init__(self,name,marks):#default constructor
        self.name=name
        self.marks=marks
        print(name,marks)
        
        print('ok')
    def display(self,x,y):# self repreasenting current obj
        self.x=x
        self.y=y
        print(x+y)
        print(self.name,self.marks)
    def check(self):
       
        print(self.x*self.y)
obj=Add('ss',55)
obj.display(6,9)
obj.check()'''

'''class Add:
    #c=10
    def __init__(self):#default constructor
        self.c=10
        self.name=input('enter name')
        self.marks=int(input('enter marks'))
        print('name',self.name)
        print('marks',self.marks)
        print('ok')
    def display(self,x,y):# self repreasenting current obj
        self.x=x
        self.y=y
        print(x+y)
        
    def check(self):
        print(self.x*self.y)
obj=Add()
obj.display(6,9)
obj.check()'''
