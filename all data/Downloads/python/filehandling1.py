'''f=open('info.txt','w')
f.write('hello john...')
f.close()'''


'''f=open('info.txt','r')
#print(f.read())
d=f.read()
f.close()
print(d)'''

#write and read
'''f=open('info.txt','w')
for i in range(3):
    a=input('enter name')
    b=input('enter age')
    f.write(a+','+b)
    f.write('\n')
f.close()


f=open('info.txt','r')
print(f.read())
#d=f.read()
f.close()
print(d)'''

'''f=open('info.txt','w')
for i in range(3):
    a=input('enter name')
    b=input('enter age')
    f.write(a+','+b)
    f.write('\n')
f.close()


f=open('info.txt','r')
#print(f.read())
d=f.readlines()
f.close()
print(d[0])'''

#readlines
'''f=open('info.txt','r')
#print(f.read())
d=f.readlines()
f.close()
for i in d:
    print(i)'''

#particular word are define
'''f=open('info.txt','r')

    
d=f.readlines()
f.close()
k=1
for i in d:
    if 'aa' in i:
         print(k,':',i)
    k=k+1'''


#header are define in write mode
'''f=open('info.txt','w')
f.write('name,age\n')
for i in range(3):
    a=input('enter name')
    b=input('enter age')
    f.write(a+','+b)
    f.write('\n')
f.close()'''

#header are define in append mode
'''f=open('info.txt','a')
f.write('name,age\n')
for i in range(3):
    a=input('enter name')
    b=input('enter age')
    f.write(a+','+b)
    f.write('\n')
f.close()'''

'''# header are not repeated.
import os
if os.path.exists('info.txt'):
    f=open('info.txt','a')
else:
    f=open('info.txt','w')
    f.write('name,age\n')
for i in range(3):
    a=input('enter name')
    b=input('enter age')
    f.write(a+','+b)
    f.write('\n')
f.close()'''

#writelines
'''import os
if os.path.exists('info.txt'):
    f=open('info.txt','a')
else:
    f=open('info.txt','w')
    f.write('name,age\n')
#writelines
data=['\nab','\nbc','\nca']    
for i in range(3):
    a=input('enter name')
    b=input('enter age')
    f.write(a+','+b)
    f.write('\n')
f.writelines(data)
f.close()'''



'''d=['hello xyz\n','hi how r u\n','ok john\n']
f=open('try.txt','w')
f.writelines(d)
f.close()


#without open read the files 
with open('try.txt') as f:
    d=f.readlines()
for i in d:
    print(i)'''


#csv (comma seprated value) file


