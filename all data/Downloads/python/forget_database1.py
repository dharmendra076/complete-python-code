from tkinter import*
import sqlite3

def save():
    conn=sqlite3.connect('log.db')
    c=conn.cursor()
    c.execute("CREATE TABLE if not exists login1(fname text,pno int)")
    a=e1.get()
    b=e2.get()
    c.execute("insert into login1 values(?,?)",(a,b))
    print('data inserted')
    conn.commit()
    conn.close() 

def display():
    conn=sqlite3.connect('log.db')
    c=conn.cursor()
    c.execute("CREATE TABLE if not exists login1(fname text,pno int)")
    c.execute('SELECT * FROM login1')
    emp1=c.fetchall()
    for i in emp1:
        print(i)
    conn.close()


def change():
    conn=sqlite3.connect('log.db')
    c=conn.cursor()
    c.execute("CREATE TABLE if not exists login1(fname text,pno int)")
    a=e3.get()
    b=e4.get()
    c.execute("UPDATE login1 SET fname = ? where pno = ?",(a,b))
    conn.commit()
    c.execute("SELECT * FROM login1")
    emp1=c.fetchall()
    for i in emp1:
        print(i)
    
    print("update one record")
    
    conn.close()
    
def delete1():
    conn=sqlite3.connect('log.db')
    c=conn.cursor()
    c.execute("CREATE TABLE if not exists login1(fname text,pno int)")
    a=e5.get()
    c.execute("DELETE from login1 where fname=?",(a,))
    
    c.execute("SELECT * FROM login1")
    emp1=c.fetchall()
    for i in emp1:
        print(i)
    
    print("delete one record")
    conn.commit()
    conn.close()

def drop1():
    conn=sqlite3.connect('log.db')
    c=conn.cursor()
    c.execute("CREATE TABLE if not exists login1(fname text,pno int)")
    c.execute("DROP TABLE login1")
    conn.commit()
    print("all record are deleted")
    emp1=c.fetchall()
    for i in emp1:
        print(i)
    conn.close()
    
t=Tk()
lab = Label(t,text="Name")
lab.place(x=100, y=150)
lab2 = Label(t,text="Phone")
lab2.place(x=100, y=200)

e1=Entry(t,text="")
e1.place(x=200,y=150)
e2=Entry(t,text="")
e2.place(x=200,y=200)


bt1=Button(t,text="SUBMIT", command=save)
bt1.place(x=150, y=250)
bt2=Button(t,text="SHOW", command=display)
bt2.place(x=250, y=250)


lab3 = Label(t,text="Udate_Name")
lab3.place(x=100, y=350)
lab4 = Label(t,text="Provide Phone No")
lab4.place(x=100, y=400)

e3=Entry(t,text="")
e3.place(x=200,y=350)
e4=Entry(t,text="")
e4.place(x=200,y=400)

bt3=Button(t,text="UPDATE", command=change)
bt3.place(x=150, y=450)


lab5 = Label(t,text="Delete")
lab5.place(x=100, y=500)

e5=Entry(t,text="")
e5.place(x=200,y=500)

bt4=Button(t,text="DELETE", command=delete1)
bt4.place(x=150, y=600)
bt5=Button(t,text="Drop", command=drop1)
bt5.place(x=250, y=600)

t.minsize(600,700)
t.mainloop()
