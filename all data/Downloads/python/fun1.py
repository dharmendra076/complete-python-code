#multiple list element 
'''for i,k in enumerate([3,4,5,6]):
    print(i,':',k)'''

'''a=[3,4,[4,5,6],5.6,['ok'],4,[50,10]]
for i in a:
    if isinstance(i,list):
        print(i)'''


#single list element
'''a=[3,4,[4,5,6],5.6,['ok'],4,[50,10]]
d=[]
for i in a:
    if isinstance(i,list):
        for j in i:
            d.append(j)
        #pass
    else:
        d.append(i)
print(d)'''

#unique number
a=[3,4,[4,5,6],5.6,['ok'],4,[50,10]]
d=[]
for i in a:
    if isinstance(i,list):
        for j in i:
            if j in d:
                continue
            d.append(j)
        #pass
    else:
        if i in d:
            continue
        d.append(i)
print(d)
