'''class A:
    def __init__(self):
        self.c=10
    def add(self):
        print('class A')
# inherit A in class B
class B(A):
    def display(self):
        print('class B')
obj=B()
obj.add()
obj.display()
print(obj.c)'''


#method overriding
'''class A:
    def __init__(self):
        self.c=10
    def add(self):
        print('class A')
# inherit A in class B
class B(A):
    def add(self):
        print('class B')
obj=B()
obj.add()
#print(obj.c)'''

#multiple inheritance
'''class A:
    def __init__(self):
        self.c=10
    def add(self):
        print('class a')
# inherit A in class B
class B(A):
    def display(self):
        print('class b')

class C(B):
    def change(self):
        print('class C')
obj=C()
obj.add()
obj.display()
obj.change()
print(obj.c)'''


'''class A:
    def __init__(self):
        self.c=10
    def add(self):
        print('class a')
# inherit A in class B
class B():
    def display(self):
        print('class b')

class C(A,B):
    def change(self):
        print('class C')
obj=C()
obj.add()
obj.display()
obj.change()
print(obj.c)'''
    


'''class A:
    def __init__(self):
        self.c=10
    def add(self):
        print('class a')
# inherit A in class B
class B():
    def add(self):
        print('class b')

#class C(A,B):
class C(B,A):
    def change(self):
        print('class C')
obj=C()
obj.add()
#obj.display()
obj.change()
print(obj.c)'''


'''class A:
    def __init__(self):
        self.c=10
# inherit A in class B
    def add(self):
        print('class b')
    def add(self,x,y):
        print(x+y)

obj=A()
obj.add(4,5)
#obj.display()
#obj.change()
#print(obj.c)'''

#overrading
'''class A:
    def __init__(self):
        self.c=10

    def add(self,x,y):
        print('class b')
    def add(self,x,y):  #last parament are calling
        print(x+y)

obj=A()
obj.add(4,5)'''


#datahidding
'''class A:
    def __init__(self):
        self.__c=10      
    def add(self,x,y):  
        print(x+y)

obj=A()
obj.add(4,5)
print(obj.c)# out side class are no calling'''


'''class A:
    def __init__(self):
        self.__c=10      
    def add(self,x,y):  
        print(self.__c)

obj=A()
obj.add(4,5)
#print(obj.__c)# out side class are no calling'''


'''class A:
    def __init__(self):
        self.__c=10      
    def add(self):  
        print(self.__c)
        print('A')
class B(A):
    def display(self):
        #print('bb')
        print(self.__c)
obj=B()
obj.display()            
obj.add()
#print(obj.c)'''

'''class A:
    def __init__(self):
        self.__c=10
        self.__add()
    def __add(self):  
        print(self.__c)
        print('A')
obj=A()'''
            
#obj.__add()

'''class A:
    def __init__(self):
        self.__c=10
        
    def __add(self):  
       
        print('A')
class B(A):
    def display(self):
        self.__add()
obj=B()
obj.display() '''        

