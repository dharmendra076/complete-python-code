import re
'''s='hello hi john abc'
m=re.search('hi',s)
#print(re.search('hi',s))
#home\12\index.html
#if found, return match object
#else none
print(m)'''


'''import re
s='hello hi john abc'
m=re.search('12',s)
#print(re.search('hi',s))
print(m)'''

'''import re
s='hello hi john abc'
m=re.search('hi',s)
#print(re.search('hi',s))
print(m.group())'''


'''import re
s='hello hi john abc'
m=re.search('hi',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''


#import re  #
'''s='hello hi john abc'
m=re.search('12',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''

'''m=re.search('\w',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''


'''s='9 hello hi john abc'
m=re.search('\w',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''

'''s='@hello hi john abc'
m=re.search('\w',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''

#+,*:match 1 or more

'''s='hello hi john abc'
m=re.search('\w*',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''

'''s='hello2 hi john abc'
m=re.search('\w+',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''


'''#s='hello#hi john abc'
s='hello2hi john abc'
m=re.search('\w*',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''


'''s='@hello hi john abc'
m=re.search('\w+',s)
#m=re.search('\w*',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''

'''s='@hello hi john abc'
m=re.search('\w+\s+\w+',s)
#m=re.search('\w\s',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''

'''s='hello hi jo@hn abc'
m=re.search('\w+@\w+',s)
#m=re.search('\d',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''

'''s=' @ hell@o hi john abc'
m=re.search('\w+@\w+',s)
#m=re.search('\d',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''

'''s='hell@o hi joh@n abc'
m=re.search('\w+@\w+',s)
#m=re.search('\d',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''


#.:read a single char
'''s='2#hell@o hi joh@n abc'
m=re.search('.',s)
#m=re.search('\d',s)
if m is not None:
    print(m.group())'''

#.*:read the all sentence
'''s='2#hell@o hi joh@n abc'
m=re.search('.*',s)
#m=re.search('\d',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''

'''s='2#hello hi john abc123@gmail.com'
m=re.search('\w+@',s)
#m=re.search('\d',s)
if m is not None:
    print(m.group())'''

#read the email id
'''s='2#hello hi john abc123@gmail.com'
m=re.search('\w+@\w+.\w+',s)
#m=re.search('\d',s)
if m is not None:
    print(m.group())'''


#\d read the single digit
'''s='hello hi john abc123@gmail.com'
m=re.search('\d',s)
#m=re.search('\d',s)
if m is not None:
    print(m.group())'''


#\d+ read the all number
'''s='2222#hello hi john abc123@gmail.com'
m=re.search('\d+',s)
#m=re.search('\d',s)
if m is not None:
    print(m.group())'''

#{} read only 10 digit number
'''s='hello 2222222222222 hi john abc123@gmail.com'
m=re.search('\d{10}',s)
#m=re.search('\d',s)
if m is not None:
    print(m.group())'''

#{,} read max digit no
'''s='hello 2222222222222 hi john abc123@gmail.com'
m=re.search('\d{10,12}',s)
#m=re.search('\d',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''


'''s='hello 011_2102222 hi  john abc123@gmail.com'
m=re.search('\d+.\d+',s)
#m=re.search('\d',s)
if m is not None:
    print(m.group())
else:
    print('not found')'''


#find cap letter
'''s='Hello hi  john abc123@gmail.com'
m=re.search('hello',s)
#m=re.search('\d',s)
if m:
    print(m.group())
else:
    print('not found')'''

#re.I ignore case
'''s='Hello hi  john abc123@gmail.com'
m=re.search('hello',s,re.I)
if m:
    print(m.group())
else:
    print('not found')'''

#match at the begging (^)
'''s='Hello hi  john abc123@gmail.com'
#m=re.search('^hi',s,re.I)
m=re.search('^hello',s,re.I)
if m:
    print(m.group())
else:
    print('not found')'''

#match at the ending ($)
'''s='Hello hi  john abc123@gmail.com ok'
#m=re.search('^hi',s,re.I)
m=re.search('ok$',s,re.I)
if m:
    print(m.group())
else:
    print('not found')'''

#read first letter
'''s='john kumar ghg'
#m=re.search('\w\s\w',s,re.I)
#m=re.search('\w+$',s,re.I)
m=re.search('\w$',s,re.I)
if m:
    print(m.group())
else:
    print('not found')'''

'''s=['hello hi john','xyz hello','hello','hello ok hello']#
for i in s:
    m=re.search('^hello$',i,re.I)
    if m:
        print(m.group())'''



'''pat=['home/abc123/index.html','home/123/index.html','home/']

for i in pat:
   
    m=re.search('^home/(\d+)/index.html',i,re.I)
    if m:
        print(m.group())'''
            
#find -
'''s='hello 222222222234566 011-2102222 hi  john abc123@gmail.com'
m=re.findall('\d+.\d+',s)
print(m)'''


#find all
'''s='hello 222222222234566 011-2102222 hi  john abc123@gmail.com'
#m=re.findall('[\d-]{10,}',s)
m=re.findall('[\d-]{10,11}',s)
print(m)'''


#find all no
'''s='hello 222222222234566 011-2102222 hi  john abc123@gmail.com'
m=re.findall('[\d-]+',s)
print(m)'''


#findall
'''s='hello 222222222234566 011_2102222 hi  john abc123@gmail.com'
m=re.findall('\d+',s)
#m=re.search('\d',s)
for i in m:
    if len(i)==10:
        print(i)'''

'''s='hello 222222222234566 011_2102222 hi  john abc123@gmail.com'
m=re.findall('\d+',s)
print(m)
#m=re.search('\d',s)
for i in m:
    if len(i)==10:
        print(i)'''
        
























