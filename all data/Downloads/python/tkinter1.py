

'''from tkinter import*
t=Tk()
t.title('My first window')
t.mainloop()'''


'''from tkinter import*
t=Tk()
#create label obj using label class
lb=Label(t,text="hello hi",font=("Arial Bold",10),bg='red')
lb.place(x=100,y=50)       
t.mainloop()'''


#window size
'''from tkinter import*
t=Tk()
#To get the default tkinter window
t.title('my first ')
t.geometry('500x500')
t.mainloop()'''


'''#Button
from tkinter import*
t=Tk()
#create label obj using label class
l=Label(t,text="hello")
l.grid(column=0,row=0)
btn=Button(t,text="Click Me")
#btn=Button(t,text="Click Me", fg='red',bg='green')
btn.grid(column=1, row=0)
t.geometry('300x200')
t.mainloop()'''


#button click event
'''from tkinter import*
def clickme():
#event in shell
    print('hello')

t=Tk()
#create label obj using label class
l=Label(t,text="click the button",font=("Arial Bold",10))
l.grid(column=0,row=0)
btn=Button(t,text="Click Me",command=clickme)
#btn=Button(t,text="Click Me", fg='red',bg='green')
btn.grid(column=1, row=0)
t.geometry('300x200')
t.mainloop()'''


#window event
'''from tkinter import*
def clickme():
   # window event
   l.configure(text='hello')

t=Tk()
#create label obj using label class
l=Label(t,text="click the button",font=("Arial Bold",10))
l.grid(column=0,row=0)
btn=Button(t,text="Click Me",command=clickme)
#btn=Button(t,text="Click Me", fg='red',bg='green')
btn.grid(column=1, row=0)
t.geometry('300x200')
t.mainloop()'''


#entry box
'''from tkinter import*
def clickme():
   # 
   s=txt.get()
   print(s)

t=Tk()
#create label obj using label class
l=Label(t,text="click the button",font=("Arial Bold",10))
l.grid(column=0,row=5)
btn=Button(t,text="Click Me",command=clickme)
#btn=Button(t,text="Click Me", fg='red',bg='green')
btn.grid(column=0, row=3)

txt=Entry(t,width=20,font=("Arial Bold",10))
txt.grid(column=1,row=0)

t.geometry('300x200')
t.mainloop()'''



'''from tkinter import*
def clickme():
   # 
   s=txt.get()
   #print(s)
   l.configure(text=s)

t=Tk()
#create label obj using label class
l=Label(t,text="",font=("Arial Bold",10))
l.grid(column=0,row=5)

btn=Button(t,text="Click Me",command=clickme)
#btn=Button(t,text="Click Me", fg='red',bg='green')
btn.grid(column=0, row=3)

txt=Entry(t,width=20,font=("Arial Bold",15))
txt.grid(column=1,row=0)

t.geometry('300x200')
t.mainloop()'''




'''from tkinter import*
from tkinter import messagebox


t=Tk()
t.title("wello")
t.geometry('350x200')

def clickme():
    
#create label obj using label class

    messagebox.showinfo('download', 'file has been deleted')

btn=Button(t,text="Click Me",command=clickme)
#btn=Button(t,text="Click Me", fg='red',bg='green')
btn.grid(column=0, row=3)
txt=Entry(t,width=20,font=("Arial Bold",15))
txt.grid(column=1,row=0)
t.geometry('300x200')
t.mainloop()'''



