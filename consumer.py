from flask import Flask
from flask_restful import Resource, Api
import pika
from threading import Thread

app = Flask(__name__)
api = Api(app)

app.config['DEBUG'] = False

data = []

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='hello')

def callback(ch, method, properties, body):
    data.append(body)
    #ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_consume(queue='hello', on_message_callback=callback,auto_ack=True)





class HelloWorld(Resource):
    def get(self):
        return {'message': data}

api.add_resource(HelloWorld, '/api/apptwo/get')

if __name__ == '__main__':
    app.run(host="localhost", port=5002)
