import pymongo
import pandas as pd
conn = pymongo.MongoClient("mongodb://localhost:27017")
dbname=conn["InvestorMF"]
collection = dbname["InvestorMFTransactions"]


x=collection.find({},{"_id":0})
table_data=[]
investor_datails=[]

for i in x:
    df = pd.DataFrame(i['Data'])
    df.sort_values("TransactionDate", inplace = True)
    table=df.loc[0:,["NAV","Units","TransactionType","TransactionDate"]]
    table_data.append(table)
    investor_datails.append({"InvestorMFId":i["InvestorMFId"],"OWId":i["OWId"]})

def calculator(table,investor_datails):
    key  = [key for key in investor_datails.keys()]
    table['Amount']=table['NAV']*table['Units']
    profit = 0
    index=table.loc[table['TransactionType'] == "SELL"]
    index=index.index.tolist()
    sale = table.loc[index]
    table = table.drop(index)
    
    if len(sale)!=0:
        for i in range(len(sale)):
            table  = table.reset_index(drop=True)
            sales = sale.iloc[i:i+1,:]['Units'].values[0]
            Current_NAV = sale.iloc[i:i+1,:]["NAV"].values[0]
            count=0
            for unit in table['Units']:
                if unit == sales:
                    profit = profit+(sales*Current_NAV)-(table['NAV'][count]*unit)
                    table.drop([count], axis=0, inplace=True)
                    break
                elif unit<sales:
                    profit=profit+(unit*Current_NAV)-(table['NAV'][count]*unit)
                    sales=sales-unit
                    table.drop([count], axis=0, inplace=True)
                elif unit>sales:
                    profit=profit+(sales*Current_NAV)-(table['NAV'][count]*sales)
                    units=unit-sales
                    table.at[count,'Units']=units
                    break 
                count+=1
                
        jsonData={}   
        table1=table["NAV"]*table["Units"]
        amt = table1.sum()
        units = table["Units"].sum()
        avg_nav = amt/units
        table["Amount"]=table["NAV"]*table["Units"]
        amount = table["Amount"].sum()
        jsonData[key[0]]=investor_datails[key[0]]
        jsonData[key[1]]=investor_datails[key[1]]
        jsonData["invested_amt"]=float('%.4f'%amount)
        jsonData["unit_bal"]=float('%.4f'%units)
        jsonData["realised_gain"]=float('%.4f'%profit)
        jsonData["avg_nav"]=float('%.4f'% avg_nav)
        #jsonData["unrealised_gain"]='%.4f'%unrealised_gain
        return jsonData
result = [calculator(x,y) for x,y in zip(table_data,investor_datails)]
result = [x for x in result if x]
print(result)