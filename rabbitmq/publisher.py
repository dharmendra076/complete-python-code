#!/usr/bin/env python
from flask import Flask
import json
import os
import pika
import logging
import sys
from flask_restful import Resource, reqparse, Api
from flask import jsonify, request, json
from subprocess import Popen

app = Flask(__name__)
api = Api(app)


class advisers(Resource):
    def post(self):
        args = request.json
        Message = 'Message' in args.keys()
        Parameters = 'parameters' in args.keys()


        #For Sending
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='localhost'))
        channel = connection.channel()
        channel = connection.channel()
        channel.queue_declare(queue='hello')
        channel.basic_publish(exchange='',
                          routing_key='hello',
                          body=args['Message'])
        connection.close()
        return f'Message successfully sent'
api.add_resource(advisers, '/')
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=50010)